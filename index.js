const fetch = require('isomorphic-fetch');

async function get(endpoint, json) {
  const options = {};

  if (json) {
    options.body = JSON.stringify(json);
    options.headers = { 
      "Content-Type": "application/json"
    } 
  }

  return await fetch(endpoint, { 
    method: "POST", 
    ...options
  });
}

module.exports = {
  create: async (opts) => {
    let options = {};

    if (typeof opts === 'string') {
      options.endpoint = opts; 
    } if (typeof opts === 'object') {
      options = opts
    }
    
    const httpClient = new FlipstarterHttpClient(options);

    if (typeof options.init == 'undefined' || options.init) {
      await httpClient.init();
    }

    return httpClient;
  }
}

class FlipstarterHttpClient {
  constructor({ endpoint }) {
    if (typeof endpoint === 'object') {
      endpoint = "http://" + endpoint.host + ":" + endpoint.port
      //TODO God willing: check if valid url and reachable? get version or something, iA.
    } else if (typeof endpoint !== 'string' || !endpoint) {
      throw new Error("invalid api endpoint");
    } else if (!endpoint.startsWith("http")) {
      endpoint = "http://" + endpoint;
    }
    
    this.endpoint = endpoint;
  }

  async init() {
    return this;
  }

  async add(payload) {

    if (typeof payload === 'object') {
      
      return await (await get(`${this.endpoint}/campaign/add`, payload)).json();

    } else {
      
      return await (await get(`${this.endpoint}/campaign/add?arg=${cid}`)).json();
    }
  }
}